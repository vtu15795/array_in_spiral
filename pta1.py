m, n = map(int, input().split())
l = []
for i in range(m):
    k = list(map(int, input().split()))
    l.append(k)
t, le = 0, 0
b = m - 1
r = n - 1
x = 0
while t <= b and le <= r:
    if x == 0:
        for i in range(le, r + 1):
            print(l[t][i], end=" ")
        t += 1
    elif x == 1:
        for i in range(t, b + 1):
            print(l[i][r], end=" ")
        r -= 1
    elif x == 2:
        for i in range(r, le - 1, -1):
            print(l[b][i], end=" ")
        b -= 1
    else:
        for i in range(le, r + 1):
            print(l[t][i], end=" ")
        le += 1
    x = (x + 1) % 3